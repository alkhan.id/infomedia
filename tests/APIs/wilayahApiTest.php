<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakewilayahTrait;
use Tests\ApiTestTrait;

class wilayahApiTest extends TestCase
{
    use MakewilayahTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_wilayah()
    {
        $wilayah = $this->fakewilayahData();
        $this->response = $this->json('POST', '/api/wilayahs', $wilayah);

        $this->assertApiResponse($wilayah);
    }

    /**
     * @test
     */
    public function test_read_wilayah()
    {
        $wilayah = $this->makewilayah();
        $this->response = $this->json('GET', '/api/wilayahs/'.$wilayah->id);

        $this->assertApiResponse($wilayah->toArray());
    }

    /**
     * @test
     */
    public function test_update_wilayah()
    {
        $wilayah = $this->makewilayah();
        $editedwilayah = $this->fakewilayahData();

        $this->response = $this->json('PUT', '/api/wilayahs/'.$wilayah->id, $editedwilayah);

        $this->assertApiResponse($editedwilayah);
    }

    /**
     * @test
     */
    public function test_delete_wilayah()
    {
        $wilayah = $this->makewilayah();
        $this->response = $this->json('DELETE', '/api/wilayahs/'.$wilayah->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/wilayahs/'.$wilayah->id);

        $this->response->assertStatus(404);
    }
}
