<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakesalesTrait;
use Tests\ApiTestTrait;

class salesApiTest extends TestCase
{
    use MakesalesTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sales()
    {
        $sales = $this->fakesalesData();
        $this->response = $this->json('POST', '/api/sales', $sales);

        $this->assertApiResponse($sales);
    }

    /**
     * @test
     */
    public function test_read_sales()
    {
        $sales = $this->makesales();
        $this->response = $this->json('GET', '/api/sales/'.$sales->id);

        $this->assertApiResponse($sales->toArray());
    }

    /**
     * @test
     */
    public function test_update_sales()
    {
        $sales = $this->makesales();
        $editedsales = $this->fakesalesData();

        $this->response = $this->json('PUT', '/api/sales/'.$sales->id, $editedsales);

        $this->assertApiResponse($editedsales);
    }

    /**
     * @test
     */
    public function test_delete_sales()
    {
        $sales = $this->makesales();
        $this->response = $this->json('DELETE', '/api/sales/'.$sales->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/sales/'.$sales->id);

        $this->response->assertStatus(404);
    }
}
