<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\sales;
use App\Repositories\salesRepository;

trait MakesalesTrait
{
    /**
     * Create fake instance of sales and save it in database
     *
     * @param array $salesFields
     * @return sales
     */
    public function makesales($salesFields = [])
    {
        /** @var salesRepository $salesRepo */
        $salesRepo = \App::make(salesRepository::class);
        $theme = $this->fakesalesData($salesFields);
        return $salesRepo->create($theme);
    }

    /**
     * Get fake instance of sales
     *
     * @param array $salesFields
     * @return sales
     */
    public function fakesales($salesFields = [])
    {
        return new sales($this->fakesalesData($salesFields));
    }

    /**
     * Get fake data of sales
     *
     * @param array $salesFields
     * @return array
     */
    public function fakesalesData($salesFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'wilayah_id' => $fake->randomDigitNotNull,
            'hp' => $fake->word,
            'email' => $fake->word,
            'ktp' => $fake->word,
            'alamat' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $salesFields);
    }
}
