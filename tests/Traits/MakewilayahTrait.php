<?php namespace Tests\Traits;

use Faker\Factory as Faker;
use App\Models\wilayah;
use App\Repositories\wilayahRepository;

trait MakewilayahTrait
{
    /**
     * Create fake instance of wilayah and save it in database
     *
     * @param array $wilayahFields
     * @return wilayah
     */
    public function makewilayah($wilayahFields = [])
    {
        /** @var wilayahRepository $wilayahRepo */
        $wilayahRepo = \App::make(wilayahRepository::class);
        $theme = $this->fakewilayahData($wilayahFields);
        return $wilayahRepo->create($theme);
    }

    /**
     * Get fake instance of wilayah
     *
     * @param array $wilayahFields
     * @return wilayah
     */
    public function fakewilayah($wilayahFields = [])
    {
        return new wilayah($this->fakewilayahData($wilayahFields));
    }

    /**
     * Get fake data of wilayah
     *
     * @param array $wilayahFields
     * @return array
     */
    public function fakewilayahData($wilayahFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'nama' => $fake->word,
            'keterangan' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s')
        ], $wilayahFields);
    }
}
