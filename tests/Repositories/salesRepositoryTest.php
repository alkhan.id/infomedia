<?php namespace Tests\Repositories;

use App\Models\sales;
use App\Repositories\salesRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakesalesTrait;
use Tests\ApiTestTrait;

class salesRepositoryTest extends TestCase
{
    use MakesalesTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var salesRepository
     */
    protected $salesRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->salesRepo = \App::make(salesRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sales()
    {
        $sales = $this->fakesalesData();
        $createdsales = $this->salesRepo->create($sales);
        $createdsales = $createdsales->toArray();
        $this->assertArrayHasKey('id', $createdsales);
        $this->assertNotNull($createdsales['id'], 'Created sales must have id specified');
        $this->assertNotNull(sales::find($createdsales['id']), 'sales with given id must be in DB');
        $this->assertModelData($sales, $createdsales);
    }

    /**
     * @test read
     */
    public function test_read_sales()
    {
        $sales = $this->makesales();
        $dbsales = $this->salesRepo->find($sales->id);
        $dbsales = $dbsales->toArray();
        $this->assertModelData($sales->toArray(), $dbsales);
    }

    /**
     * @test update
     */
    public function test_update_sales()
    {
        $sales = $this->makesales();
        $fakesales = $this->fakesalesData();
        $updatedsales = $this->salesRepo->update($fakesales, $sales->id);
        $this->assertModelData($fakesales, $updatedsales->toArray());
        $dbsales = $this->salesRepo->find($sales->id);
        $this->assertModelData($fakesales, $dbsales->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sales()
    {
        $sales = $this->makesales();
        $resp = $this->salesRepo->delete($sales->id);
        $this->assertTrue($resp);
        $this->assertNull(sales::find($sales->id), 'sales should not exist in DB');
    }
}
