<?php namespace Tests\Repositories;

use App\Models\wilayah;
use App\Repositories\wilayahRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakewilayahTrait;
use Tests\ApiTestTrait;

class wilayahRepositoryTest extends TestCase
{
    use MakewilayahTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var wilayahRepository
     */
    protected $wilayahRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->wilayahRepo = \App::make(wilayahRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_wilayah()
    {
        $wilayah = $this->fakewilayahData();
        $createdwilayah = $this->wilayahRepo->create($wilayah);
        $createdwilayah = $createdwilayah->toArray();
        $this->assertArrayHasKey('id', $createdwilayah);
        $this->assertNotNull($createdwilayah['id'], 'Created wilayah must have id specified');
        $this->assertNotNull(wilayah::find($createdwilayah['id']), 'wilayah with given id must be in DB');
        $this->assertModelData($wilayah, $createdwilayah);
    }

    /**
     * @test read
     */
    public function test_read_wilayah()
    {
        $wilayah = $this->makewilayah();
        $dbwilayah = $this->wilayahRepo->find($wilayah->id);
        $dbwilayah = $dbwilayah->toArray();
        $this->assertModelData($wilayah->toArray(), $dbwilayah);
    }

    /**
     * @test update
     */
    public function test_update_wilayah()
    {
        $wilayah = $this->makewilayah();
        $fakewilayah = $this->fakewilayahData();
        $updatedwilayah = $this->wilayahRepo->update($fakewilayah, $wilayah->id);
        $this->assertModelData($fakewilayah, $updatedwilayah->toArray());
        $dbwilayah = $this->wilayahRepo->find($wilayah->id);
        $this->assertModelData($fakewilayah, $dbwilayah->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_wilayah()
    {
        $wilayah = $this->makewilayah();
        $resp = $this->wilayahRepo->delete($wilayah->id);
        $this->assertTrue($resp);
        $this->assertNull(wilayah::find($wilayah->id), 'wilayah should not exist in DB');
    }
}
