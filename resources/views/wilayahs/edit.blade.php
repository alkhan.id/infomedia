@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Wilayah
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($wilayah, ['route' => ['wilayahs.update', $wilayah->id], 'method' => 'patch']) !!}

                        @include('wilayahs.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection