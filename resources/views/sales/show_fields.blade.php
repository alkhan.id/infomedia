<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $sales->id !!}</p>
</div>

<!-- Nama Field -->
<div class="form-group">
    {!! Form::label('nama', 'Nama:') !!}
    <p>{!! $sales->nama !!}</p>
</div>

<!-- Wilayah Id Field -->
<div class="form-group">
    {!! Form::label('wilayah_id', 'Wilayah Id:') !!}
    <p>{!! $sales->wilayah_id !!}</p>
</div>

<!-- Hp Field -->
<div class="form-group">
    {!! Form::label('hp', 'Hp:') !!}
    <p>{!! $sales->hp !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $sales->email !!}</p>
</div>

<!-- Ktp Field -->
<div class="form-group">
    {!! Form::label('ktp', 'Ktp:') !!}
    <p>{!! $sales->ktp !!}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', 'Alamat:') !!}
    <p>{!! $sales->alamat !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $sales->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $sales->updated_at !!}</p>
</div>

