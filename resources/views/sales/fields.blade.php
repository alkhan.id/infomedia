<!-- Nama Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nama', 'Nama:') !!}
    {!! Form::text('nama', null, ['class' => 'form-control']) !!}
</div>

<!-- Wilayah Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('wilayah_id', 'Wilayah:') !!}
    <?php
        $wilayahs = App\Models\wilayah::get();
    ?>
    <select name="wilayah_id" class"form-control">
    @foreach($wilayahs as $wilayah)
        <option value="{{$wilayah->id}}">{{$wilayah->nama}}</option>
    @endforeach
    </select>

</div>

<!-- Hp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('hp', 'Hp:') !!}
    {!! Form::text('hp', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Ktp Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ktp', 'Ktp:') !!}
    {!! Form::text('ktp', null, ['class' => 'form-control']) !!}
</div>

<!-- Alamat Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('alamat', 'Alamat:') !!}
    {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('sales.index') !!}" class="btn btn-default">Cancel</a>
</div>
