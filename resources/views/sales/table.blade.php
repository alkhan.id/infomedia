<div class="table-responsive">
    <table class="table" id="sales-table">
        <thead>
            <tr>
                <th>Nama</th>
        <th>Wilayah</th>
        <th>Hp</th>
        <th>Email</th>
        <th>Ktp</th>
        <th>Alamat</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($sales as $sales)
            <tr>
                <td>{!! $sales->nama !!}</td>
            <td>
            {{App\Models\wilayah::where('id', $sales->wilayah_id)->first()->nama}}
            </td>
            <td>{!! $sales->hp !!}</td>
            <td>{!! $sales->email !!}</td>
            <td>{!! $sales->ktp !!}</td>
            <td>{!! $sales->alamat !!}</td>
                <td>
                    {!! Form::open(['route' => ['sales.destroy', $sales->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('sales.show', [$sales->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('sales.edit', [$sales->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
