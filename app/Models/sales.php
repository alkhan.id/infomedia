<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class sales
 * @package App\Models
 * @version August 1, 2019, 9:22 am UTC
 *
 * @property string nama
 * @property integer wilayah_id
 * @property string hp
 * @property string email
 * @property string ktp
 * @property string alamat
 */
class sales extends Model
{
    use SoftDeletes;

    public $table = 'sales';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama',
        'wilayah_id',
        'hp',
        'email',
        'ktp',
        'alamat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'wilayah_id' => 'integer',
        'hp' => 'string',
        'email' => 'string',
        'ktp' => 'string',
        'alamat' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required',
        'wilayah_id' => 'required',
        'hp' => 'required',
        'email' => 'required',
        'ktp' => 'required',
        'alamat' => 'required'
    ];

    
}
