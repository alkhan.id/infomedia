<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class product
 * @package App\Models
 * @version August 1, 2019, 9:18 am UTC
 *
 * @property string kode
 * @property string nama
 * @property string jenis
 * @property integer qty
 * @property string keteranganan
 */
class product extends Model
{
    use SoftDeletes;

    public $table = 'products';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'kode',
        'nama',
        'jenis',
        'qty',
        'keteranganan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kode' => 'string',
        'nama' => 'string',
        'jenis' => 'string',
        'qty' => 'integer',
        'keteranganan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kode' => 'required',
        'nama' => 'required',
        'jenis' => 'required',
        'qty' => 'required',
        'keteranganan' => 'required'
    ];

    
}
