<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class wilayah
 * @package App\Models
 * @version August 1, 2019, 9:20 am UTC
 *
 * @property string nama
 * @property string keterangan
 */
class wilayah extends Model
{
    use SoftDeletes;

    public $table = 'wilayahs';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nama',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama' => 'string',
        'keterangan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama' => 'required',
        'keterangan' => 'required'
    ];

    
}
