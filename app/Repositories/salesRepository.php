<?php

namespace App\Repositories;

use App\Models\sales;
use App\Repositories\BaseRepository;

/**
 * Class salesRepository
 * @package App\Repositories
 * @version August 1, 2019, 9:22 am UTC
*/

class salesRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'wilayah_id',
        'hp',
        'email',
        'ktp',
        'alamat'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return sales::class;
    }
}
