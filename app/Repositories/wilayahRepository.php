<?php

namespace App\Repositories;

use App\Models\wilayah;
use App\Repositories\BaseRepository;

/**
 * Class wilayahRepository
 * @package App\Repositories
 * @version August 1, 2019, 9:20 am UTC
*/

class wilayahRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return wilayah::class;
    }
}
