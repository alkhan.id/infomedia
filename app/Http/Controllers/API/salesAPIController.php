<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatesalesAPIRequest;
use App\Http\Requests\API\UpdatesalesAPIRequest;
use App\Models\sales;
use App\Repositories\salesRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class salesController
 * @package App\Http\Controllers\API
 */

class salesAPIController extends AppBaseController
{
    /** @var  salesRepository */
    private $salesRepository;

    public function __construct(salesRepository $salesRepo)
    {
        $this->salesRepository = $salesRepo;
    }

    /**
     * Display a listing of the sales.
     * GET|HEAD /sales
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $sales = $this->salesRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($sales->toArray(), 'Sales retrieved successfully');
    }

    /**
     * Store a newly created sales in storage.
     * POST /sales
     *
     * @param CreatesalesAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatesalesAPIRequest $request)
    {
        $input = $request->all();

        $sales = $this->salesRepository->create($input);

        return $this->sendResponse($sales->toArray(), 'Sales saved successfully');
    }

    /**
     * Display the specified sales.
     * GET|HEAD /sales/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var sales $sales */
        $sales = $this->salesRepository->find($id);

        if (empty($sales)) {
            return $this->sendError('Sales not found');
        }

        return $this->sendResponse($sales->toArray(), 'Sales retrieved successfully');
    }

    /**
     * Update the specified sales in storage.
     * PUT/PATCH /sales/{id}
     *
     * @param int $id
     * @param UpdatesalesAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatesalesAPIRequest $request)
    {
        $input = $request->all();

        /** @var sales $sales */
        $sales = $this->salesRepository->find($id);

        if (empty($sales)) {
            return $this->sendError('Sales not found');
        }

        $sales = $this->salesRepository->update($input, $id);

        return $this->sendResponse($sales->toArray(), 'sales updated successfully');
    }

    /**
     * Remove the specified sales from storage.
     * DELETE /sales/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var sales $sales */
        $sales = $this->salesRepository->find($id);

        if (empty($sales)) {
            return $this->sendError('Sales not found');
        }

        $sales->delete();

        return $this->sendResponse($id, 'Sales deleted successfully');
    }
}
