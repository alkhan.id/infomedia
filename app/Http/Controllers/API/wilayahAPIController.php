<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatewilayahAPIRequest;
use App\Http\Requests\API\UpdatewilayahAPIRequest;
use App\Models\wilayah;
use App\Repositories\wilayahRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class wilayahController
 * @package App\Http\Controllers\API
 */

class wilayahAPIController extends AppBaseController
{
    /** @var  wilayahRepository */
    private $wilayahRepository;

    public function __construct(wilayahRepository $wilayahRepo)
    {
        $this->wilayahRepository = $wilayahRepo;
    }

    /**
     * Display a listing of the wilayah.
     * GET|HEAD /wilayahs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $wilayahs = $this->wilayahRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($wilayahs->toArray(), 'Wilayahs retrieved successfully');
    }

    /**
     * Store a newly created wilayah in storage.
     * POST /wilayahs
     *
     * @param CreatewilayahAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatewilayahAPIRequest $request)
    {
        $input = $request->all();

        $wilayah = $this->wilayahRepository->create($input);

        return $this->sendResponse($wilayah->toArray(), 'Wilayah saved successfully');
    }

    /**
     * Display the specified wilayah.
     * GET|HEAD /wilayahs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var wilayah $wilayah */
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            return $this->sendError('Wilayah not found');
        }

        return $this->sendResponse($wilayah->toArray(), 'Wilayah retrieved successfully');
    }

    /**
     * Update the specified wilayah in storage.
     * PUT/PATCH /wilayahs/{id}
     *
     * @param int $id
     * @param UpdatewilayahAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatewilayahAPIRequest $request)
    {
        $input = $request->all();

        /** @var wilayah $wilayah */
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            return $this->sendError('Wilayah not found');
        }

        $wilayah = $this->wilayahRepository->update($input, $id);

        return $this->sendResponse($wilayah->toArray(), 'wilayah updated successfully');
    }

    /**
     * Remove the specified wilayah from storage.
     * DELETE /wilayahs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var wilayah $wilayah */
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            return $this->sendError('Wilayah not found');
        }

        $wilayah->delete();

        return $this->sendResponse($id, 'Wilayah deleted successfully');
    }
}
