<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatewilayahRequest;
use App\Http\Requests\UpdatewilayahRequest;
use App\Repositories\wilayahRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class wilayahController extends AppBaseController
{
    /** @var  wilayahRepository */
    private $wilayahRepository;

    public function __construct(wilayahRepository $wilayahRepo)
    {
        $this->wilayahRepository = $wilayahRepo;
    }

    /**
     * Display a listing of the wilayah.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $wilayahs = $this->wilayahRepository->all();

        return view('wilayahs.index')
            ->with('wilayahs', $wilayahs);
    }

    /**
     * Show the form for creating a new wilayah.
     *
     * @return Response
     */
    public function create()
    {
        return view('wilayahs.create');
    }

    /**
     * Store a newly created wilayah in storage.
     *
     * @param CreatewilayahRequest $request
     *
     * @return Response
     */
    public function store(CreatewilayahRequest $request)
    {
        $input = $request->all();

        $wilayah = $this->wilayahRepository->create($input);

        Flash::success('Wilayah saved successfully.');

        return redirect(route('wilayahs.index'));
    }

    /**
     * Display the specified wilayah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            Flash::error('Wilayah not found');

            return redirect(route('wilayahs.index'));
        }

        return view('wilayahs.show')->with('wilayah', $wilayah);
    }

    /**
     * Show the form for editing the specified wilayah.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            Flash::error('Wilayah not found');

            return redirect(route('wilayahs.index'));
        }

        return view('wilayahs.edit')->with('wilayah', $wilayah);
    }

    /**
     * Update the specified wilayah in storage.
     *
     * @param int $id
     * @param UpdatewilayahRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatewilayahRequest $request)
    {
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            Flash::error('Wilayah not found');

            return redirect(route('wilayahs.index'));
        }

        $wilayah = $this->wilayahRepository->update($request->all(), $id);

        Flash::success('Wilayah updated successfully.');

        return redirect(route('wilayahs.index'));
    }

    /**
     * Remove the specified wilayah from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $wilayah = $this->wilayahRepository->find($id);

        if (empty($wilayah)) {
            Flash::error('Wilayah not found');

            return redirect(route('wilayahs.index'));
        }

        $this->wilayahRepository->delete($id);

        Flash::success('Wilayah deleted successfully.');

        return redirect(route('wilayahs.index'));
    }
}
